function diemUuTienKhuVuc(){
    var diemKhuVuc = document.getElementById("khuVuc").value;
    if(diemKhuVuc == 0){
        return 0;
    } else if(diemKhuVuc == 1){
        return 2;
    } else if(diemKhuVuc == 2){
        return 1.5;
    } else if(diemKhuVuc == 3){
        return 0.5;
    }
}
function diemUuTienDoiTuong(){
    var diemDoiTuong = document.getElementById("doiTuong").value;
    if(diemDoiTuong == 0){
        return 0;
    } else if(diemDoiTuong == 1){
        return 2.5;
    } else if(diemDoiTuong == 2){
        return 1.5;
    } else if(diemDoiTuong == 3){
        return 1;
    }
}

function tinhKetQua(){
    var diemChuan = document.getElementById("diemChuan").value*1;
    var diemToan = document.getElementById("diemToan").value*1;
    var diemVan = document.getElementById("diemVan").value*1;
    var diemAnh = document.getElementById("diemAnh").value*1;
    var diemKV = diemUuTienKhuVuc();
    var diemDT = diemUuTienDoiTuong();
    var tongDiem = (diemToan + diemVan + diemAnh + diemKV + diemDT);
    if(tongDiem >= diemChuan && diemToan > 0 && diemVan > 0 && diemAnh > 0){  
        document.getElementById("hienThi").innerHTML = `Tổng điểm là: ${tongDiem} - Thí sinh đậu`
    }else if(tongDiem < diemChuan && diemToan > 0 && diemVan > 0 && diemAnh > 0){
        document.getElementById("hienThi").innerHTML = `Tổng điểm là: ${tongDiem} - Thí sinh rớt`
    }else{
        document.getElementById("hienThi").innerHTML = `Tổng điểm là: ${tongDiem} - Thí sinh rớt và có 1 môn 0đ`
    }
}

//bai2
function tinhTienDien(){
    var KwDauTien50 = 500;
    var KwTiepTheo50 = 650;
    var KwTiepTheo100 = 850;
    var KwTiepTheo150 = 1100;
    var KwConLai = 1300;
    var hoTen = document.getElementById("hoTen").value;
    var soKw = document.getElementById("soKw").value*1;
    var tongTien = 0;
    if(soKw <= 50){
         tongTien = soKw * KwDauTien50;
    }else if(soKw <= 100){
        tongTien = (50 * KwDauTien50) + ((soKw - 50) * KwTiepTheo50);
    }else if(soKw <= 200){
        tongTien = (50 * KwDauTien50) + (50 * KwTiepTheo50 ) + ((soKw - 100) * KwTiepTheo100);
    }else if(soKw <= 350){
        tongTien = (50 * KwDauTien50) + (50 * KwTiepTheo50 ) + (100 * KwTiepTheo100) + ((soKw -200) * KwTiepTheo150);
    }else if(soKw > 350){
        tongTien = (50 * KwDauTien50) + (50 * KwTiepTheo50 ) + (100 * KwTiepTheo100) + (150 * KwTiepTheo150) + ((soKw - 350) * KwConLai);
    }
    document.getElementById("hienThiB2").innerHTML = `Họ tên: ${hoTen} - Tổng tiền: ${tongTien}vnd`;
}  

//bai 3
function tinhThueThuNhap(){
    var hoVaTen = document.getElementById("hoVaTen").value;
    var tongThuNhap = document.getElementById("thuNhap").value*1;
    var soNguoi = document.getElementById("soNguoi").value*1;
    var thuNhapChiuThue = tongThuNhap - 4e+6 - soNguoi * 1.6e+6;
    var thueThuNhapCaNhan = 0;
    if(tongThuNhap == "" || tongThuNhap*1 <0){
        return document.getElementById("hienThiB3").innerHTML = `Họ tên: ${hoVaTen}-Tổng thu nhập năm bạn nhập chưa đúng! Mời bạn nhập lại!`;
    }else if(thuNhapChiuThue <= 60e+6){
        thueThuNhapCaNhan = thuNhapChiuThue * 0.05;
    }else if(thuNhapChiuThue <= 120e+6){
        thueThuNhapCaNhan = ((thuNhapChiuThue - 60e+6) * 0.1) + (60e+6 * 0.05);
    }else if(thuNhapChiuThue <= 210e+6){
        thueThuNhapCaNhan = (60e+6 * 0.05) + (60e+6 * 0.1) + ((thuNhapChiuThue - 120e+6) * 0.15);
    }else if(thuNhapChiuThue <= 384e+6){
        thueThuNhapCaNhan = (60e+6 * 0.05) + (60e+6 * 0.1) + (90e+6 * 0.15) + ((thuNhapChiuThue - 210e+6) * 0.2);
    }else if(thuNhapChiuThue <= 624e+6){
        thueThuNhapCaNhan = (60e+6 * 0.05) + (60e+6 * 0.1) + (90e+6 * 0.15) + (174e+6 * 0.2) + ((thuNhapChiuThue - 384e+6) * 0.25);
    }else if(thuNhapChiuThue <= 960e+6){
        thueThuNhapCaNhan = (60e+6 * 0.05) + (60e+6 * 0.1) + (90e+6 * 0.15) + (174e+6 * 0.2) + (240e+6 * 0.25) + ((thuNhapChiuThue - 624e+6) * 0.3);
    }else{
        thueThuNhapCaNhan = (60e+6 * 0.05) + (60e+6 * 0.1) + (90e+6 * 0.15) + (174e+6 * 0.2) + (240e+6 * 0.25) + (336e+6 * 0.3) + ((thuNhapChiuThue - 960e+6) * 0.35);
    }
    return document.getElementById("hienThiB3").innerHTML = `Họ tên: ${hoVaTen} - Thuế thu nhập cá nhân của bạn là: ${thueThuNhapCaNhan}VND.`;
}
//bai 4
function tinhHoaDon(){
    var maKhachHang = document.getElementById("maKhachHang").value;
    var soKenhCaoCap = document.getElementById("soKenhCaoCap").value*1;
    var loai = document.querySelector("#chonKhachHang").value;
    var hoaDon = 0;
    if(loai == 1){
        var phiHoaDon = 4.5;
        var dichVuCoBan = 20.5;
        var kenhCaoCap = 7.5;
        hoaDon = phiHoaDon + dichVuCoBan + (kenhCaoCap * soKenhCaoCap);
    }else if(loai == 2){
        var soKetNoi = document.getElementById("soKetNoi").value*1;
        var phiHoaDon = 15;
        var dichVuCoBan = 75;
        var kenhCaoCap = 50;
        if(soKetNoi <= 10){
        hoaDon = phiHoaDon + dichVuCoBan + (kenhCaoCap * soKenhCaoCap);
        }else{
            var phiHoaDon = 15;
            var dichVuCoBan = 75 + ((soKetNoi -10)*5);
            var kenhCaoCap = 50;
            hoaDon = phiHoaDon + dichVuCoBan + (kenhCaoCap * soKenhCaoCap);
        }
    }
    return document.getElementById("hienThiB4").innerHTML = `Mã khách hàng: ${maKhachHang} -Tổng tiền hóa đơn: ${hoaDon}$`;
} 
document.querySelector("#soKetNoi").style.display = "none";
document.querySelector("#chonKhachHang").onchange = function () {
  var loai = document.querySelector("#chonKhachHang").value;
  if (loai == 0) {
    document.querySelector("#soKetNoi").style.display = "none";
  } else if(loai == 1) {
    document.querySelector("#soKetNoi").style.display = "none";
  } else{
    document.querySelector("#soKetNoi").style.display = "block";
  }
}
